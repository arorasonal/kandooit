# Copyright 2020 VentorTech OU
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0).

{
    "name": "Frame Data Import",
    "summary": """
        Frame Data Import
        """,
    "author": "Kandooit",
    "website": "",
    "category": "",
    "license": "LGPL-3",
    "version": "13.0.1.0.0",
    "images": [
        # "static/description/Two_factor_authentification.png",
    ],
    "installable": True,
    "depends": [
        "opt_custom"
    ],
    "data": [
        "views/res_config_settings_views.xml",
        "views/frame_data_import_view.xml",
        "views/frame_data_widget_views.xml",
        
    ],
    'qweb': ["static/src/xml/dashboard.xml"],
    "external_dependencies": {
        "python": [
            
        ],
    },
}
