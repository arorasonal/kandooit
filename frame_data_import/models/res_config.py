# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
#import pandas as pd
import requests
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils
import json
ROUNDING_FACTOR = 16

class ResConfigSettings(models.TransientModel):
    _inherit = 'res.config.settings'

    account_number = fields.Char("Account")
    # is_twilio = fields.Boolean("Twilio")
    zipcode = fields.Char("Zipcode")
    location = fields.Char("Locations")
    username = fields.Char("Username")
    password = fields.Char("Password")