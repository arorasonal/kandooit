# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
#import pandas as pd
import requests
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils
import json
ROUNDING_FACTOR = 16

class FeameDataImport(models.TransientModel):
    _name = 'frame.data.import'

    manufacture = fields.Char("Manufacture")
    brand = fields.Char("Brand")
    collections = fields.Char("Collections")
    status = fields.Char("Collections")
    last_synch = fields.Char("Last Synch")