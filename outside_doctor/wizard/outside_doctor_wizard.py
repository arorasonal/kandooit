# Copyright 2020 VentorTech OU
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0).

from logging import getLogger
from datetime import date,datetime,timedelta
import calendar
from dateutil.rrule import rrule, DAILY
import requests
import json
from odoo import fields, models, _
from odoo.exceptions import AccessError,UserError


_logger = getLogger(__name__)

class OutsideDoctorImportWizard(models.TransientModel):
    _name = 'outside.doctor.import.wizard'

    l_name = fields.Char('Last Name',index=True)
    city = fields.Char("City")
    state = fields.Char("State")
    limit = fields.Integer("Limit", default=200)
    import_line_id = fields.One2many('doctor.import.line.wizard','import_id','Import')

    def all_select(self):
        view = self.env.ref('outside_doctor.view_outside_doctor_import_wizard_form')
        for line in self.import_line_id:
            line.check_bool = True
        return {
            'name': _('Outside Doctor'),
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'outside.doctor.import.wizard',
            'views': [(view.id, 'form')],
            'view_id': view.id,
            'target': 'new',
            'res_id': self.id,
        }

    def find_outside_doctor_import_screen(self):
        self.import_line_id.unlink()
        view = self.env.ref('outside_doctor.view_outside_doctor_import_wizard_form')
        api = "https://npiregistry.cms.hhs.gov/api/"
        params = {}
        headers = {
            'Content-type':'application/json',
            'Accept':'application/json'
        }
        if self.city:
            params.update({'city': self.city})
        params.update({'last_name': self.l_name, 'state_id': self.state, 'version': 2.1, 'limit':self.limit})
        print('params================', params)
        try:
            response = requests.request("GET", api, params=params).text
            response_data = json.loads(response)
            if response_data and response_data.get('results'):
                dict_data = self.prepare_data(response_data)
            return {
                'name': _('Outside Doctor'),
                'type': 'ir.actions.act_window',
                'view_mode': 'form',
                'res_model': 'outside.doctor.import.wizard',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'res_id': self.id,
            }
        except Exception as e:
            raise UserError(_('%s')%(e))


    def create_outside_doctor_profile(self):
        outside_doctor_obj = self.env['outside.doctor.class']
        country_obj = self.env['res.country']
        state_obj = self.env['res.country.state']
        exist_rec = []
        dict_data = {'exist_lst': [], 
                'data_lst': [], 
                'checked_rec': [lines for lines in self.import_line_id if lines.check_bool]
                }
        #checked_rec = [lines for lines in self.import_line_id if lines.check_bool]
        if all(lines.check_bool == False for lines in self.import_line_id):
            raise UserError(_("Please select record!" ))
        for line in self.import_line_id:
            if line.check_bool:
                outside_dc_exist = outside_doctor_obj.search([('npi', '=', line.npi)])
                if outside_dc_exist:
                    dict_data['exist_lst'].append(outside_dc_exist)
                if not outside_dc_exist:
                    vals = {}
                    if line.country:
                        country_data = line.country.split('/')
                        country = country_data[0]
                        state = country_data[1]
                        country_id = country_obj.search([('code', '=', country)], limit=1)
                        state_id = state_obj.search([('country_id', '=', country_id.id), ('code', '=', state)], limit=1)
                        vals.update({'country_id': country_id.id, 'state_id': state_id.id})

                    vals.update({
                        'credential':line.credential, 'prefix': line.name, 'f_name': line.f_name,'l_name': line.l_name, 'm_name': line.m_name,
                        'street': line.street, 'street2': line.street2,'city': line.city, 'phone': line.phone,
                        'npi': line.npi,'npi_type': line.npi_type, 'taxonomies': line.taxonomies,'license_doctor': line.license_doctor, 'medicaid': line.medicaid,
                        'state': state
                        })
                    doctor_id = outside_doctor_obj.create(vals)
                    dict_data['data_lst'].append(doctor_id.id)
        if len(dict_data['exist_lst']) == len(dict_data['checked_rec']):
            raise UserError(_("Provider already exists." ))
        if dict_data['data_lst'] :
            action = self.env.ref('outside_doctor.outside_doctor_action').read()[0]
            if len(dict_data['data_lst']) > 1:
                action['domain'] = [('id', 'in', dict_data['data_lst'])]
            elif len(dict_data['data_lst']) == 1:
                form_view = [(self.env.ref('outside_doctor.outside_doctor_listing_form_view').id, 'form')]
                if 'views' in action:
                    action['views'] = form_view + [(state,view) for state,view in action['views'] if view != 'form']
                else:
                    action['views'] = form_view
                action['res_id'] = dict_data['data_lst'][0]
            return action
            
    def prepare_data(self, response_data):
        import_line_obj = self.env['doctor.import.line.wizard']
        for data in response_data['results']:
            city = ''
            phone = ''
            taxonomies = ''
            vals = {}
            if 'last_name' in data['basic']:
                vals.update({'l_name': data['basic']['last_name']})
            if 'first_name' in data['basic']:
                vals.update({'f_name': data['basic']['first_name']})
            if 'middle_name' in data['basic']:
                vals.update({'m_name': data['basic']['middle_name']})
            if 'credential' in data['basic']:
                vals.update({'credential': data['basic']['credential']})
            if 'number' in data:
                vals.update({'npi': data['number']})
            if 'enumeration_type' in data:
                enumeration_type = 'Individual' if data['enumeration_type'] == 'NPI-1' else "Group"
                if enumeration_type == 'Individual':
                    vals.update({'npi_type': 'individual'})
                else:
                    vals.update({'npi_type': 'group'})
            if 'name_prefix' in data['basic']:
                vals.update({'name': data['basic']['name_prefix']})
            for address in data.get('addresses'):
                if address.get('address_purpose') == 'LOCATION':
                    if 'country_code' in address and address.get('country_code') not in [None, False, 'None', 'False']:
                        country = address.get('country_code') + "/" + address.get('state')
                        vals.update({'country': country})
                    if 'city' in address and address.get('city') not in [None, False, 'None', 'False']:
                        city = address.get('city')
                        vals.update({'city': city})
                    if 'telephone_number' in address and address.get('telephone_number') not in [None, False, 'None', 'False']:
                        phone = address.get('telephone_number')
                        vals.update({'phone': phone})
                    if 'address_1' in address and address.get('address_1') not in [None, False, 'None', 'False']:
                        street = address.get('address_1')
                        vals.update({'street': street})
                    if 'address_2' in address and address.get('address_2') not in [None, False, 'None', 'False']:
                        street2 = address.get('address_2')
                        vals.update({'street2': street2})
            if 'taxonomies' in data and data.get('taxonomies') not in [None, False, 'None', 'False']:
                for taxo in data.get('taxonomies'):
                    code = taxo.get('code')
                    if 'code' in taxo:
                        vals.update({'taxonomies': code})
                    if 'license' in taxo:
                        vals.update({'license_doctor': taxo.get('license')})
                vals.update({'import_id': self.id})
            if 'identifiers' in data and data.get('identifiers') not in [None, False, 'None', 'False']:
                for taxo in data.get('identifiers'):
                    identifier = taxo.get('identifier')
                    if 'identifier' in taxo:
                        vals.update({'medicaid': identifier})
                vals.update({'import_id': self.id})
            import_line_obj.create(vals)
            #return vals

class O_DoctorImportLineWizard(models.TransientModel):
    _name = 'doctor.import.line.wizard'

    name = fields.Char("Name")
    l_name = fields.Char("Last Name")
    f_name = fields.Char("First Name")
    m_name = fields.Char("Middle Name")
    street = fields.Char("Street")
    street2 = fields.Char("Street2")
    license_doctor = fields.Char("License")
    medicaid = fields.Char("Medicaid")
    credential = fields.Char("Credential")
    city = fields.Char("City")
    phone = fields.Char("Phone")
    country = fields.Char("Country")
    taxonomies = fields.Char("Taxonomy")
    npi_type = fields.Char("NPI Type")
    npi = fields.Char("NPI")
    import_id = fields.Many2one('outside.doctor.import.wizard', 'import')
    check_bool = fields.Boolean("Select")










