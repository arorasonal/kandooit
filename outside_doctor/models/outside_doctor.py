# Copyright 2020 VentorTech OU
# License LGPL-3.0 or later (https://www.gnu.org/licenses/lgpl-3.0).

from base64 import b32encode, b64encode
from os import remove, urandom
from tempfile import mkstemp
from logging import getLogger
from datetime import date,datetime
from contextlib import suppress

from odoo import fields, models, _
from odoo.exceptions import AccessError
from odoo.http import request
from random import randint


class OutsideDoctor(models.Model):
    _name = 'outside.doctor.class'

    prefix = fields.Char('Prefix',index=True)
    name = fields.Char('Name', compute='get_full_name')
    f_name = fields.Char('First Name',index=True)
    m_name = fields.Char('Middle Name',index=True)
    l_name = fields.Char('Last Name',index=True)
    credential = fields.Char("Credential")
    phone = fields.Char("Phone")
    mobile = fields.Char("Mobile")
    taxonomies = fields.Char("Taxonomy")
    code = fields.Char("Code")
    license_doctor = fields.Char("License")
    medicaid = fields.Char("Medicaid")
    email = fields.Char("Email")
    website = fields.Char("Website")
    #npi_type = fields.Char("NPI Type")
    npi = fields.Char("NPI")
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state = fields.Char('State')
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict')
    comment = fields.Text(string='Notes')
    #npi_type1 = fields.Selection([(0, 'Individual'), (1, 'Group')], 'Type', default=0)
    npi_type = fields.Selection([
        ('individual', 'Individual'),
        ('group', 'Group'),
        ], string='Type', default='individual')



    def get_full_name(self):
        for rec in self:
            rec.name = ("%s" + " " + "%s")%(rec.l_name if rec.l_name else '', rec.f_name if rec.f_name else '')