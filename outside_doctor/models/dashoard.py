# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import timedelta, datetime, date
from dateutil.relativedelta import relativedelta
#import pandas as pd
import requests
from pytz import utc
from odoo import models, fields, api, _
from odoo.http import request
from odoo.tools import float_utils
import json
ROUNDING_FACTOR = 16

class HrEmployee(models.Model):

    _inherit = 'hr.employee'

    def emoployee_inside_doctor_class(self):
        res = {
            'type': 'ir.actions.client',
            'name':'Employee Import Data',
            'tag':'outside_doctor',
            'context':{'is_employee':1}
        }
        print('res++++++++',res)
        return res

    @api.model
    def import_data(self):
        print('test$$$$$$$$$$$$$$$$$$$$$$$')

    @api.model
    def get_user_employee_details(self):
        uid = request.session.uid
        employee = self.env['hr.employee'].sudo().search_read([('user_id', '=', uid)], limit=1)
        print("employee",employee)
        return employee

    @api.model
    def get_api_data(self, data):
        ctx = data
        api = "https://npiregistry.cms.hhs.gov/api/"
        params = {}
        headers = {
            'Content-type':'application/json',
            'Accept':'application/json'
        }
        if ctx.get('city'):
            params.update({'city': ctx.get('city')})
        params.update({'last_name': ctx.get('last_name'), 'state_id': ctx.get('state'), 'version': 2.1, 'limit':200})
        #try:
        response = requests.request("GET", api, params=params).text
        response_data = json.loads(response)
        if response_data and response_data.get('results'):
            dict_data = self.prepare_data(response_data)
            return dict_data

    def doctor_class(self,datas):
        datas = datas[0]
        print('datasdatasdatasdatasdatasdatasdatas',datas)
        outside_doctor_obj = self.env['outside.doctor.class']
        country_obj = self.env['res.country']
        state_obj = self.env['res.country.state']
        dict_data = {'exist_lst': [], 
            'data_lst': [], 
            'checked_rec': [datas]
            }
        flag = False
        if not datas['data']:
            return {'message': "Please Select Record!"} 
        for line in datas['data']:
            print('line=++++++++++++',line)
            outside_dc_exist = outside_doctor_obj.search([('npi', '=', line.get('npi'))])
            if outside_dc_exist:
                dict_data['exist_lst'].append(outside_dc_exist)
            if not outside_dc_exist:
                vals = {}
                if line.get('country'):
                    country_data = line.get('country').split('/')
                    country = country_data[0]
                    state = country_data[1]
                    country_id = country_obj.search([('code', '=', country)], limit=1)
                    state_id = state_obj.search([('country_id', '=', country_id.id), ('code', '=', state)], limit=1)
                    vals.update({'country_id': country_id.id, 'state_id': state_id.id})
                vals.update({
                    'zip': line.get('zip'),'state': state,
                    'credential':line.get('credential'), 'prefix': line.get('prefix'), 'f_name': line.get('f_name'),'l_name': line.get('l_name'), 'm_name': line.get('m_name'),
                    'street': line.get('street'), 'street2': line.get('street2'),'city': line.get('city'), 'phone': line.get('phone'),
                    'npi': line.get('npi'),'npi_type': line.get('npi_type'), 'taxonomies': line.get('taxonomies'),'license_doctor': line.get('license_doctor'), 'medicaid': line.get('medicaid'),
                    # 'state': state
                })
                print('hhhhhhhhhhhhhhhh',line.get('zip'),line.get('prefix'))
                doctor_id = outside_doctor_obj.create(vals)
                dict_data['data_lst'].append(doctor_id.id)
                flag = True
        if len(dict_data['exist_lst']) == len(dict_data['checked_rec']):
            return {'message': "Provider already exists."}
        if flag:
            print('flagggggggggggggggg')
            return {'data': dict_data['data_lst']}
        else:
            return {'message': "Provider already exists."}

    def import_employee(self,datas):
        datas = datas[0]
        print('datasdatasdatasdatasdatasdatasdatas',datas)
        employee_obj = self.env['hr.employee']
        country_obj = self.env['res.country']
        state_obj = self.env['res.country.state']
        dict_data = {'exist_lst': [], 
            'data_lst': [], 
            'checked_rec': [datas]
            }
        flag = False
        if not datas['data']:
            return {'message': "Please Select Record!"} 
        for line in datas['data']:
            print('line=++++++++++++',line)
            inside_dc_exist = employee_obj.search([('npi', '=', line.get('npi'))])
            if inside_dc_exist:
                dict_data['exist_lst'].append(inside_dc_exist)
            if not inside_dc_exist:
                vals = {}
                if line.get('country'):
                    country_data = line.get('country').split('/')
                    country = country_data[0]
                    state = country_data[1]
                    country_id = country_obj.search([('code', '=', country)], limit=1)
                    state_id = state_obj.search([('country_id', '=', country_id.id), ('code', '=', state)], limit=1)
                    # vals.update({'country_id': country_id.id, 'state_id': state_id.id})
                vals.update({
                    'pin': line.get('zip'),'name': line.get('f_name'),
                    'first_name': line.get('f_name'),'last_name': line.get('l_name'),
                    'work_phone': line.get('phone'),
                    'npi': line.get('npi'),'npi_type': line.get('npi_type'), 'taxonomy': line.get('taxonomies'),'license': line.get('license_doctor'),
                    # 'state': state
                })
                print('hhhhhhhhhhhhhhhh',line.get('zip'),line.get('prefix'))
                doctor_id = inside_dc_exist.create(vals)
                dict_data['data_lst'].append(doctor_id.id)
                flag = True
        if len(dict_data['exist_lst']) == len(dict_data['checked_rec']):
            return {'message': "Provider already exists."}
        if flag:
            print('flagggggggggggggggg')
            return {'data': dict_data['data_lst']}
        else:
            return {'message': "Provider already exists."}


    @api.model
    def create_data(self, *args, **kwargs):
        # print('current_context++++++++++', kwargs, args[0]['context_data']['is_employee'])
        if 'is_employee' not in  args[0]['context_data']:
            return self.doctor_class(args)
        else:
            print("***********************")
            return self.import_employee(args)

    def prepare_data(self, response_data):
        import_line_obj = self.env['doctor.import.line.wizard']
        lst= []
        sequence = 1
        for data in response_data['results']:
            city = ''
            phone = ''
            taxonomies = ''
            vals = {}
            if 'last_name' in data['basic']:
                vals.update({'l_name': data['basic']['last_name']})
            if 'first_name' in data['basic']:
                vals.update({'f_name': data['basic']['first_name']})
            if 'middle_name' in data['basic']:
                vals.update({'m_name': data['basic']['middle_name']})
            if 'credential' in data['basic']:
                vals.update({'credential': data['basic']['credential']})
            if 'number' in data:
                vals.update({'npi': data['number']})
            if 'enumeration_type' in data:
                enumeration_type = 'Individual' if data['enumeration_type'] == 'NPI-1' else "Group"
                if enumeration_type == 'Individual':
                    vals.update({'npi_type': 'individual'})
                else:
                    vals.update({'npi_type': 'group'})
            if 'name_prefix' in data['basic']:
                vals.update({'prefix': data['basic']['name_prefix']})
            for address in data.get('addresses'):
                if address.get('address_purpose') == 'LOCATION':
                    if 'state' in address and address.get('state') not in [None, False, 'None', 'False']:
                        vals.update({'state': address.get('state')})
                    if 'postal_code' in address and address.get('postal_code') not in [None, False, 'None', 'False']:
                        vals.update({'zip': address.get('postal_code')})
                    if 'country_code' in address and address.get('country_code') not in [None, False, 'None', 'False']:
                        country = address.get('country_code') + "/" + address.get('state')
                        vals.update({'country': country})
                    if 'city' in address and address.get('city') not in [None, False, 'None', 'False']:
                        city = address.get('city')
                        vals.update({'city': city})
                    if 'telephone_number' in address and address.get('telephone_number') not in [None, False, 'None', 'False']:
                        phone = address.get('telephone_number')
                        vals.update({'phone': phone})
                    if 'address_1' in address and address.get('address_1') not in [None, False, 'None', 'False']:
                        street = address.get('address_1')
                        vals.update({'street': street})
                    if 'address_2' in address and address.get('address_2') not in [None, False, 'None', 'False']:
                        street2 = address.get('address_2')
                        vals.update({'street2': street2})
            if 'taxonomies' in data and data.get('taxonomies') not in [None, False, 'None', 'False']:
                for taxo in data.get('taxonomies'):
                    if taxo.get('primary'):
                        if 'code' in taxo:
                            vals.update({'taxonomies': taxo.get('code')})
                        if 'license' in taxo:
                            vals.update({'license_doctor': taxo.get('license')})
            if 'identifiers' in data and data.get('identifiers') not in [None, False, 'None', 'False']:
                for taxo in data.get('identifiers'):
                    identifier = taxo.get('identifier')
                    if 'identifier' in taxo:
                        vals.update({'medicaid': identifier})
            vals.update({'sequence_id': sequence})
            lst.append(vals)
            sequence = sequence + 1
        print('lst++++++++++++++++++',lst)
        return lst
