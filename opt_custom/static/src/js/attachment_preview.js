odoo.define('opt_custom.attachmentPreview', function (require) {
    "use strict";

    var DocumentViewer = require('mail.DocumentViewer');
    var field_registry = require('web.field_registry');
    var relational_fields = require('web.relational_fields');

    var AttachmentPreviewMultiFiles = relational_fields.FieldMany2ManyBinaryMultiFiles.extend({
        template_files: "AttachmentPreviewMultiFiles",
        events: {
            'click #preview': '_onAttachPreiview',
        },
        _onAttachPreiview:function(ev){
            ev.preventDefault();
            ev.stopPropagation();
            if(this.value && this.value.data && this.value.data[0] && this.value.data[0].data && this.value.data[0].data.id){
                this.attachmentIDs =  _.pluck(this.value.data, 'data');
                var activeAttachmentID = this.value.data[0].data.id;
                if (activeAttachmentID) {
                    var attachmentViewer = new DocumentViewer(this, this.attachmentIDs, activeAttachmentID);
                    attachmentViewer.appendTo($('body'));
                }
            }
        },
    });
    field_registry.add("attachment_preview",AttachmentPreviewMultiFiles);

});
