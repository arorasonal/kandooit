# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Optical ERP',
    'category': '',
    'author': 'Serpent Consulting Services Pvt. Ltd.',
    'version': '1.0',
    'description': """
This module used to manage optician business
==========================================================================

Optical ERP module useful to manage:

1. Frame segment
2. Lens segment

""",
    'website': 'https://serpentcs.com',
    'depends': ['contacts', 'website_sale', 'stock', 'auth_oauth', 'auth_signup', 'hr', 'calendar', 'account','web_widget_multi_image', 'base_geolocalize'],
    'data': ['security/optical_security.xml',
             'security/ir.model.access.csv',
             'data/sequence.xml',
             'data/activities.xml',
             'data/gender.xml',
             'data/region.xml',
             'data/contact_lens_manufacturer.xml',
             'data/insurance_active_cron_view.xml',
             'wizard/accessory_change_cost_price_view.xml',
             'wizard/copy_cms_setting_view.xml',
             'views/frame_view.xml',
             'views/partner_view.xml',
             'views/signup_extend_templates.xml',
             'views/lens_view.xml',
             'views/contact_lens.xml',
             'views/accessory_view.xml',
             'views/service_view.xml',
             'views/insurance_view.xml',
             'views/hr_employee_view.xml',
             'views/company_view.xml',
             'views/template.xml',
             'views/calendar_views.xml',
             'report/report_appointment_view.xml'],
    'demo': [],
    'qweb': ['static/src/xml/calender_view.xml',
             'static/src/xml/attachment_preview.xml'],
    'auto_install': False,
    'installable': True
}
