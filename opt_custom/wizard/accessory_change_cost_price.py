# -*- coding: utf-8 -*-

from odoo import fields, models
from odoo.exceptions import ValidationError


class AccessoryChangeCostPrice(models.TransientModel):
    """Accessory Change Cost Price"""
    _name = "spec.accessorry.change.cost.price"
    _description = "Asseory Cost Price change"

    currency_id = fields.Many2one('res.currency', string="currency", readonly="1",
    					default=lambda self: self.env.user.company_id.currency_id)
    new_price = fields.Monetary(string="Cost")

    def action_change_price(self):
        """Method to Change Accessory Cost Price."""
        active_id = self._context.get('active_id')
        accessory_id = self.env['product.template'].browse(active_id)

        if self.new_price <= 0:
                raise ValidationError("Price shouble not be 0 or negative!")
        else:
            accessory_id.cost_price = self.new_price
