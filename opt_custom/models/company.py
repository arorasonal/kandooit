# -*- coding: utf-8 -*-

from odoo import fields, models, api, _
from odoo.exceptions import Warning
from odoo.addons.base.models.res_partner import _tz_get


class ResCompnay(models.Model):
    _inherit = "res.company"

    npi = fields.Char(string="NPI")
    contact_person = fields.Char(string="Contact Person")
    provide = fields.Many2one('hr.employee', string='Provider')
    timezone = fields.Selection(_tz_get, string='Timezone')
    dst_observed = fields.Boolean(string="DST Observed")
    main = fields.Boolean(string="Main")
    region_ids = fields.Many2many('spec.region', 'company_region_rel', 'company_id', 'region_id', string="Region")
    hours = fields.Many2one(
        'resource.calendar', string="Hours")

    @api.constrains('main')
    def check_main(self):
        if self.main:
            company_id = self.env['res.company'].search([('id', '!=', self._origin.id),('main', '=', True)])
            if company_id:
                    raise Warning(_('%s company is allready main.' % (company_id.name)))
