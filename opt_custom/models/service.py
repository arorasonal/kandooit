# -*- coding: utf-8 -*-

from odoo import fields, models


class Service(models.Model):
    _inherit = "product.template"

    ser_pro_code_id = fields.Many2one('spec.procedure.code', string='Procedure')
    ser_modifier_id = fields.Many2one('spec.lens.modifier', string='Modifier')
