# -*- coding: utf-8 -*-

from odoo import api, fields, models, _
from odoo.exceptions import ValidationError, Warning
from datetime import date, datetime
import re
from dateutil.relativedelta import relativedelta
import phonenumbers
from email.policy import default


class ResPartner(models.Model):
    _inherit = 'res.partner'

    first_name = fields.Char(string="Frist Name")
    middle_name = fields.Char(string="Middle Name")
    last_name = fields.Char(string="Last Name")
    nick_name = fields.Char(string="Nickname")
    title = fields.Selection([('mr', 'Mr'), ('mrs', 'Mrs'),('ms', 'Ms'),('miss', 'Miss'),('mx', 'Mx')], string="Title")
    date_of_birth = fields.Date(string='Date of Birth')
    age = fields.Char(compute="_get_age", string="Age")
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'),
                               ('unspecified', 'Unspecified')], default="male", string="Gender")
    gender_identification = fields.Selection([('identifies_as_male', 'Identifies as Male'), ('identifies_as_female', 'Identifies as Female'),
                                            ('ftm_transgender_male', 'Female-to-Male (FTM)/Transgender Male'), ('mtf_transgender_female', 'Male-to-Female (MTF)/Transgender Female'),
                                            ('identifies_conforming_gender', 'Identifies as non-conforming gender'),
                                            ('additional_gender_category_other,', 'Additional gender category or other,'), ('choose_not_disclose', 'Choose not to disclose')
                                            ], string="Gender Identification")
    suffix = fields.Selection([('jr', 'Jr'), ('sr', 'Sr'), ('i', 'I'), ('ii', 'II'),
                               ('iii', 'III'), ('iv', 'IV'), ('v', 'V'), ('vi', 'VI'),
                               ('esq', 'Esq'), ('cpa', 'CPA'),
                               ('dc', 'DC'), ('dds', 'DDS'),
                               ('vm', 'VM')], string="Suffix")
    ssn = fields.Char(string="SSN")
    other = fields.Char(string="Other")
    marital_status = fields.Selection([('single', 'Single'), ('married', 'Married'),
                                       ('Divorced', 'Divorced'), ('widowed', 'Widowed')], string="Marital Status")
    communication_preferences = fields.Selection([('email', 'Email'), ('phone', 'Phone'), ('cell', 'Cell'), ], string="Preferred Contact Method")
    preferred_language = fields.Selection([('declined_to_Specify', 'Declined to Specify'), ('unspecified', 'Unspecified'),
                                           ('english', 'English'), ('spanish', 'Spanish'), ('french', 'French'),
                                           ('german', 'German')], string="Preferred Language")
    relationship_to_patient = fields.Selection([('father', 'Father'), ('mother', 'Mother'), ('son', 'Son'), ('daughter', 'Daughter'),('husband', 'Husband'), ('wife', 'Wife'),
                                                ('partner', 'Partner'), ('parent', 'Parent'), ('sibling', 'Sibling'), ('other', 'Other')], string="Relationship to Patient")
    hipaa_sign = fields.Boolean(string="HIPAA Signature on file")
    date = fields.Date(string="Date")
    provide = fields.Many2one('hr.employee', string='Preferred Provider')
    partner_id = fields.Many2one('res.partner', string='Responsible Party')
    preferred_location = fields.Many2one('res.company', string='Preferred Location', default=lambda self: self.env.user.company_id.currency_id)
    deceased = fields.Boolean(string="Deceased")
    patient = fields.Boolean(string="Patient")
    country_id = fields.Many2one('res.country', string="Country")
    document_ids = fields.One2many('multi.images', 'partner_id', string="Attachment")
    notes_ids = fields.One2many('spec.notes', 'partner_id', string="Notes")
    insurance_ids = fields.One2many('spec.insurance', 'partner_id', string="Insurance")
    insurance_authorizations_ids = fields.One2many('spec.insurance.authorizations', 'partner_id', string="Insurance Authorizations")
    recall_type_ids = fields.One2many('spec.recall.type.line', 'partner_id', string="Recall Type")
    next_recall = fields.Date(string="Next Recall")
    family_ids = fields.Many2many('res.partner', 'res_partner_rel', 'partner_id',
                                  'res_id', string='Family')
    update_label_address = fields.Boolean(string="Update Label Address")
    update_label_cell = fields.Boolean(string="Update Label cell")
    update_label_other = fields.Boolean(string="Update Label other")
    disabled_email = fields.Boolean(string="Disabled email")
    contact_lens_ids = fields.One2many('spec.contact.lenses', 'partner_id', string="Rx")
    occupation = fields.Char(string="Occupation")
    ethnicity = fields.Selection([('declined_to_Specify', 'Declined to Specify'), ('american_indian_Alaska_native', 'American Indian or Alaska Native'),
                                  ('asian', 'Asian'), ('black_or_african_american', 'Black or African American'), ('native _pacific_islander', 'Native Hawaiian or Other Pacific Islander'),
                                  ('white', 'White')], string="Ethnicity")
    race = fields.Selection([('declined', 'Declined'), ('american_indian', 'American Indian'), ('asian', 'Asian'),
                             ('native _pacific_islander', 'Native Hawaiian or Other Pacific Islander'), ('black_or_african_american', 'Black or African American'),
                             ('white', 'White'), ('other', 'Other')], string="Race")
    referred_by = fields.Selection([('patient', 'Patient'), ('family', 'Family'), ('doctor', 'Doctor'), ('advertising', 'Advertising'), ('facebook', 'Facebook'),
                                    ('Google', 'google'), ('drive_by', 'Drive By'),], string="Referred By")
    emergency_name = fields.Char(string="Emergency Name")
    emergency_phone = fields.Char(string="Emergency Phone")
    select_all = fields.Boolean(string='Select all')
    communication_ids = fields.One2many('spec.communication.table', 'partner_id', string="Communication Method")
    assign_as_emergency_contact = fields.Boolean(string='Assign As Emergency Contact')
    assign_as_responsible_party = fields.Boolean(string='Assign As Responsible Party')

    @api.onchange('first_name', 'middle_name', 'last_name')
    def _onchange_name(self):
        if self.patient:
            name = ''
            if self.last_name:
                name += self.last_name + ' ' or ''
            if self.middle_name:
                name += self.middle_name + ' ' or ''
            if self.first_name:
                name += self.first_name + ' ' or ''
            self.name = name

    @api.onchange('is_company')
    def _onchange_is_company(self):
        if self.patient and self.is_company:
            self.company_type = 'person'

    @api.onchange('hipaa_sign')
    def _onchange_hipaa_sign(self):
        if self.hipaa_sign:
            self.date = fields.Date.today()
        else:
            self.date = False

    @api.onchange('phone', 'mobile', 'emergency_phone')
    def _onchange_phone(self):
        if self.phone and self.phone.isdigit():
            self.phone = phonenumbers.format_number(phonenumbers.parse(self.phone, 'US'),
                                                    phonenumbers.PhoneNumberFormat.NATIONAL)
        if self.emergency_phone and self.emergency_phone.isdigit():
            self.emergency_phone = phonenumbers.format_number(phonenumbers.parse(self.emergency_phone, 'US'),
                                                    phonenumbers.PhoneNumberFormat.NATIONAL)

    @api.onchange('street', 'street2', 'city', 'state_id', 'zip', 'country_id')
    def _onchange_street(self):
        if self.family_ids.ids:
            for family_ids in self.family_ids.ids:
                family_id = self.env['res.partner'].browse(family_ids)
                family_id.street = self.street or ''
                family_id.street2 = self.street2 or ''
                family_id.city = self.city or ''
                family_id.state_id = self.state_id or ''
                family_id.zip = self.zip or ''
                family_id.country_id = self.country_id or ''

    @api.onchange('disabled_email')
    def _onchange_disabled_email(self):
        if self.disabled_email:
            self.email = False

    @api.onchange('select_all')
    def _onchange_select_all(self):
        if self.select_all:
            for communication_ids in self.communication_ids.ids:
                communication_id = self.env['spec.communication.table'].browse(communication_ids)
                communication_id.text = True
                communication_id.cell = True
                communication_id.email = True
                communication_id.mail = True
                communication_id.opt_out = True

    @api.model
    def default_get(self, fields):
        defaults = super(ResPartner, self).default_get(fields)
        country_id = self.env.ref('base.us')
        defaults['country_id'] = country_id.id
        communication_list = ['Appointment', 'Recall', 'Oder Pick-up', 'General']
        dic_list = []
        for communication in communication_list:
            dic_list.append((0,0, {'communication': communication}))
        defaults['communication_ids'] = dic_list
        return defaults

    @api.depends('date_of_birth',)
    def _get_age(self):
        """Age Method"""
        for record in self:
            record.age = '0 y 0 m'
            if record.date_of_birth:
                age = relativedelta(date.today(), datetime.strptime(
                    str(record.date_of_birth), "%Y-%m-%d").date())
                record.age = str(age.years) + ' y ' + \
                    str(age.months) + ' m'

    @api.constrains('email')
    def check_email(self):
        if self.email:
            match = re.match(
                '^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', self.email)
            if not match:
                raise ValidationError('Please Enter Valid Email')

    @api.constrains('ssn')
    def check_last_char(self):
        total_size = 0
        if self.ssn:
            total_size = len(self.ssn)
            if total_size == 9:
                self.ssn = '{}-{}-{}'.format(self.ssn[:3],
                                             self.ssn[3:5], self.ssn[5:])
            ssn = str(self.ssn).split('-')
            for total_char in ssn:
                total_size = len(total_char)
            if total_size != 4 and total_size != 9:
                raise ValidationError(
                    _("PLEASE UPDATE SSN TO EITHER 4 OR 9 DIGITS"))


class CommunicationTable(models.Model):
    _name = "spec.communication.table"
    _description = 'Communication Table'

    text = fields.Boolean(string='Text')
    cell = fields.Boolean(string='cell')
    email = fields.Boolean(string='Email')
    mail = fields.Boolean(string='mail')
    opt_out = fields.Boolean(string='Opt-out')
    communication = fields.Char(string='Communication')
    partner_id = fields.Many2one('res.partner', string="Partner")


class ActivitiesList(models.Model):
    _name = 'spec.activities.list'
    _description = 'Activities list'
    name = fields.Char(string="Activities")


class CommunicationMethods(models.Model):
    _name = 'spec.communication.method'
    _description = 'Communication Method'

    name = fields.Char(string='Communication Method')
    text = fields.Boolean(string="Text")


class Document(models.Model):
    _name = 'spec.documents'
    _description = 'Documents'
    name = fields.Char(string="Name")
    document_type = fields.Selection([('cms_1500', 'CMS-1500 Form'), ('credit_card_authorization', 'Credit Card Authorization'),
                                      ("driver_license", "Driver's License"), ('hipaa_npp', 'HIPAA NPP'), (
        'id_card', 'ID Card'), ('insurance_card', 'Insurance Card'),
        ('insurance_card_secondary', 'Insurance Card - Secondary'), ('insurance_card_tertiary', 'Insurance Card - Tertiary'), ('pcp_consent_form', 'PCP Release Consent Form'),
        ('patient_photo', 'Patient Photo'), ('payment', 'Payment'), ('registration_form', 'Registration Form'),
        ('registration_packet', 'Registration Packet'), ('release_of_information', 'Release of Information'),
        ('reminder_call_release', 'Reminder Call Release'), ('service_agreement', 'Service Agreement'),
        ('cataract_referral_letter', 'Cataract Referral Letter'), ('contact_lens_agreement', 'Contact Lens Agreement'),
        ('diabetic_letter', 'Diabetic Letter'), ('insurance_uthorization', 'Insurance Authorization'),
        ('invoice','Invoice'), ('medical_history', 'Medical History'), ('other', 'Other'),
        ('office_policies', 'Office Policies'), ('outside_rx', 'Outside Rx'), ('pcp_letter', 'PCP Letter'),
        ('previous_exams', 'Previous Exams'), ('referral', 'Referral'), ('ssn_card', 'SSN Card')], string="Type")
    comments = fields.Text(string="Notes")
    upload_file = fields.Binary(string="Attachments")
    upload_file_file_name = fields.Char(string='File Name')
    date = fields.Date(string="Date", default=fields.Date.today())
    user_id = fields.Many2one('res.users', string='User', default=lambda self: self.env.user)
    partner_id = fields.Many2one('res.partner', string="Partner")

    @api.constrains('upload_file')
    def check_upload_file(self):
        """Check Upload File"""
        for record in self:
            if record.upload_file_file_name:
                file_name = record.upload_file_file_name.split('.')
                if file_name[-1].lower() not in ['png', 'jpg','jpeg', 'gif', 'tif', 'pdf']:
                    raise ValidationError(
                        "You can upload only PDF, PNG, JPG, GIF or TIF Documnet file!")


class MultiImages(models.Model):
    _inherit = "multi.images"

    partner_id = fields.Many2one('res.partner', string="Partner")
    document_type = fields.Selection([('cms_1500', 'CMS-1500 Form'), ('credit_card_authorization', 'Credit Card Authorization'),
                                      ("driver_license", "Driver's License"), ('hipaa_npp', 'HIPAA NPP'),
                                      ('id_card', 'ID Card'), ('insurance_card', 'Insurance Card'),
                                      ('insurance_card_secondary', 'Insurance Card - Secondary'),
                                      ('insurance_card_tertiary', 'Insurance Card - Tertiary'),
                                      ('pcp_consent_form', 'PCP Release Consent Form'),
                                      ('patient_photo', 'Patient Photo'),
                                      ('payment', 'Payment'), ('registration_form', 'Registration Form'),
                                      ('registration_packet', 'Registration Packet'),
                                      ('release_of_information', 'Release of Information'),
                                      ('reminder_call_release', 'Reminder Call Release'),
                                      ('service_agreement', 'Service Agreement'),
                                      ('cataract_referral_letter', 'Cataract Referral Letter'),
                                      ('contact_lens_agreement', 'Contact Lens Agreement'),
                                      ('diabetic_letter', 'Diabetic Letter'),
                                      ('insurance_uthorization', 'Insurance Authorization'),
                                      ('invoice', 'Invoice'), ('medical_history', 'Medical History'),
                                      ('other', 'Other'), ('office_policies', 'Office Policies'),
                                      ('outside_rx', 'Outside Rx'), ('pcp_letter', 'PCP Letter'),
                                      ('previous_exams', 'Previous Exams'),
                                      ('referral', 'Referral'), ('ssn_card', 'SSN Card')], string="Type")
    upload_file_file_name = fields.Char(string='File Name')
    date = fields.Date(string="Date", default=fields.Date.today())
    user_id = fields.Many2one('res.users', string='User', default=lambda self: self.env.user)
    attachment_ids = fields.Many2many('ir.attachment', string='Attachment', required=True, help='Add Attachment.')
    attachment_preview = fields.Char('Attachment Preview')
    
    @api.constrains('image')
    def check_upload_file(self):
        """Check Upload File"""
        for record in self:
            if record.upload_file_file_name:
                file_name = record.upload_file_file_name.split('.')
                if file_name[-1].lower() not in ['png', 'jpg','jpeg', 'gif', 'tif', 'pdf']:
                    raise ValidationError(
                        "You can upload only PDF, PNG, JPG, GIF or TIF Documnet file!")
                    

class Notes(models.Model):
    _name = 'spec.notes'
    _description = 'Notes'
    name = fields.Text(string="Notes")
    notes_type = fields.Char(string="Type", default="Profile")
    urgent = fields.Boolean(string="Urgent")
    follow_up = fields.Boolean(string="Follow Up")
    partner_id = fields.Many2one('res.partner', string="Partner")
    date = fields.Date(string="Date", default=fields.Date.today())
    user_id = fields.Many2one('res.users', string='User', default=lambda self: self.env.user)


class Insurance(models.Model):
    _name = 'spec.insurance'
    _description = 'Insurance'
    _rec_name = 'carrier_id'

    active = fields.Boolean(string="Active", default=True)
    sequence = fields.Char(string='Insurance ID')
    name = fields.Char(string="Subscriber Name")
    use_patient_ssn = fields.Boolean(string="Use Patient SSN")
    relationship = fields.Selection([('child_depende', 'Child/Depende'), ('domestic', 'Domestic'), ('partner', 'Partner'), ('other', 'Other'),
                                     ('self', 'Self'), ('spouse', 'Spouse'), ('student', 'Student')], string="Relationship")
    address_same_patient = fields.Boolean(string='Address Same as Patient')
    address_line_1 = fields.Char(string='Address Line 1')
    address_line_2 = fields.Char(string='Address Line 2')
    city = fields.Char(string='City')
    zip = fields.Char(change_default=True, string="ZIP")
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', help='The name of the state.')
    gender = fields.Selection([('male', 'Male'), ('female', 'Female'), ('unspecified', 'Unspecified')], default="male", string="Gender")
    phone = fields.Char(string="Phone")
    date = fields.Date(string="Date of Birth")
    employer = fields.Char(string="Employer")
    carrier_id = fields.Many2one('spec.insurance.company.listing', string="Insurance")
    plan_id = fields.Many2one('spec.insurance.plan', string="Plan")
    priority = fields.Selection([('primary', 'Primary'), ('secondary', 'Secondary'), ('tertiary', 'Tertiary')], string="Priority")
    insurance_type = fields.Selection([('medical', 'Medical'), ('vision', 'Vision')], string="Insurance Type")
    address = fields.Char(string="Address")
    activation_date = fields.Date(string="Active", default=fields.Date.today())
    termination_date = fields.Date(string="Termination")
    partner_id = fields.Many2one('res.partner', string="Partner")

    @api.model
    def create(self, vals):
        if vals and not vals.get('sequence', False):
            vals['sequence'] = self.env[
                'ir.sequence'].next_by_code('spec.insurance.id')
        return super(Insurance, self).create(vals)

    @api.onchange('use_patient_ssn')
    def _onchange_name(self):
        if self.use_patient_ssn:
            self.sequence = self.partner_id.ssn or ''
        else:
            self.sequence = ''

    @api.onchange('address_same_patient')
    def _onchange_address_same_patient(self):
        if self.address_same_patient:
            self.address_line_1 = self.partner_id.street
            self.address_line_2 = self.partner_id.street2
            self.city = self.partner_id.city
            self.zip = self.partner_id.zip
            self.state_id = self.partner_id.state_id
        else:
            self.address_line_1 = False
            self.address_line_2 = False
            self.city = False
            self.zip = False
            self.state_id = False

    @api.onchange('carrier_id')
    def _onchange_carrier_id(self):
        if self.carrier_id:
            self.address = self.carrier_id.address_line_1

    @api.onchange('relationship')
    def _onchange_relationship(self):
        if self.relationship == 'self':
            self.name = self.partner_id.name
            self.address_line_1 = self.partner_id.street
            self.address_line_2 = self.partner_id.street2
            self.city = self.partner_id.city
            self.state_id = self.partner_id.state_id
            self.zip = self.partner_id.zip
            self.gender = self.partner_id.gender
            self.phone = self.partner_id.phone
            self.date = self.partner_id.date_of_birth
        else:
            self.name = False
            self.address_line_1 = False
            self.address_line_2 = False
            self.city = False
            self.state_id = False
            self.zip = False
            self.gender = False
            self.phone = False
            self.date = False


class InsuranceAuthorizations(models.Model):
    _name = 'spec.insurance.authorizations'
    _description = 'Insurance Authorizations'

    authorizations_type = fields.Selection([('authorization', 'Authorization'), ('referral', 'Referral')], string="Vision Medical")
    insurance_id = fields.Many2one('spec.insurance', string="Insurance")
    plan_id = fields.Many2one('spec.insurance.plan', string="Plan")
    authorizations_number = fields.Integer(string="Authorizations Number")
    authorizations_date = fields.Date(string='Effective Date', default=fields.Date.today())
    expiration_date = fields.Date(string='Expiration Date')
    employee_id = fields.Many2one('hr.employee', string="Verified By")
    name = fields.Char(string="Name")
    address = fields.Char(string="Address")
    phone = fields.Char(string="Phone")
    exam = fields.Boolean(string="Exam")
    farme = fields.Boolean(string="Farme")
    lenses = fields.Boolean(string="Lenses")
    contact_lens = fields.Boolean(string="Contact Lens")
    vision_medical = fields.Selection([('vision', 'Vision'), ('medical', 'Medical')], string="Vision Medical", default='vision')
    """Vision Fileds"""
    exam_copay = fields.Monetary(string="Exams")
    medical_copay = fields.Monetary(string="Medical Co-pay")
    frame_allowance = fields.Monetary(string="Frame")
    frame_range = fields.Monetary(string="Frame Range")
    frame_range_to = fields.Monetary(string="Frame Range")
    contact_lens_allowance = fields.Monetary(string="Contact Lens")
    total_allowance = fields.Monetary(string="Total Allowance")
    post_contact_glasses = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Post Cataract Glasses")
    surgery_date = fields.Date(string="Surgery Date")
    referring_provider = fields.Many2one('hr.employee', string="Referring Provider")
    """Medical"""
    office_copay = fields.Monetary(string="Office Copay")
    deductible_amount = fields.Monetary(string="Deductible Amount")
    remaining_deductible_amount = fields.Monetary(string="Remaining Deductible Amount")
    specialist_deductible = fields.Monetary(string="Specialist Deductible")
    family_deductible_amount = fields.Monetary(string="Family Deductible Amount")
    family_remaining_deductible = fields.Monetary(string="Family Remaining Deductible")
    co_insurance = fields.Monetary(string="Co-insurance")
    out_of_pocket_max = fields.Monetary(string="Out of Pocket Max")
    remaining_out_pocket_max = fields.Monetary(string="Remaining Out of Pocket")
    cataract_co_manage = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="Cataract Co-Manage")
    cataract_referring_provider = fields.Many2one('hr.employee', string="Referring Provider")
    cataract_surgery_date = fields.Date(string="Surgery Date")
    cataract_asm = fields.Date(string="ASM")
    cataract_rql = fields.Date(string="RQL")
    cataract_second_surgery = fields.Boolean(string="Second Surgery")
    yag_co_manage = fields.Selection([('yes', 'Yes'), ('no', 'No')], string="YAG Co-Manage")
    yag_surgery_date = fields.Date(string="Surgery Date")
    yag_referring_provider = fields.Many2one('hr.employee', string="Referring Provider")
    yag_asm = fields.Date(string="ASM")
    yag_rql = fields.Date(string="RQL")
    yag_second_surgery = fields.Boolean(string="Second Surgery")
    currency_id = fields.Many2one('res.currency', string="currency", default=lambda self: self.env.user.company_id.currency_id, readonly="1")
    """Referral Fields"""
    referring_physician = fields.Many2one('hr.employee', string="Referring Physician")
    referring_physician_npi = fields.Char(string="Referring Physician NPI")
    referring_physician_tel = fields.Char(string="Referring Physician Tel")
    date_of_vist = fields.Date(string="Date of Visit")
    number_of_vist = fields.Char(string="Number of Visit")
    procedure_code = fields.Char(string="Procedure Code")
    diagnosis_code = fields.Char(string="Diagnosis Code")
    notes = fields.Text(string="Notes")
    partner_id = fields.Many2one('res.partner', string="Partner")

    @api.onchange('authorizations_date')
    def _onchange_authorizations_date(self):
        if self.authorizations_date:
            self.expiration_date = self.authorizations_date + relativedelta(day=30)

    @api.onchange('insurance_id')
    def _onchange_insurance_id(self):
        if self.insurance_id:
            self.plan_id = self.insurance_id.plan_id

    @api.constrains('vision_medical')
    def check_exam(self):
        if self.authorizations_type == 'authorization' and self.vision_medical == 'vision':
            if not self.exam and not self.farme and not self.lenses and not self.contact_lens:
                raise ValidationError('Please Select Email')

    @api.onchange('referring_physician_tel')
    def _onchange_referring_physician_tel(self):
        if self.referring_physician_tel and self.referring_physician_tel.isdigit():
            self.referring_physician_tel = phonenumbers.format_number(phonenumbers.parse(self.referring_physician_tel, 'US'),
                                                                      phonenumbers.PhoneNumberFormat.NATIONAL)


class RecallType(models.Model):
    _name = 'spec.recall.type'
    _description = 'Recall Type'

    name = fields.Char(string="Recall Type")
    months = fields.Integer(string="Months to Recall")
    recall_schedule_ids = fields.One2many(
        'spec.recall.schedule', 'recall_type_id', string="Recall Schedule")


class RecallTypeLine(models.Model):
    _name = 'spec.recall.type.line'
    _description = 'Recall Type Line'

    name = fields.Many2one('spec.recall.type', string="Recall Type")
    months = fields.Integer(related="name.months", string="Months to Recall")
    next_recall_date = fields.Date(string="Date")
    partner_id = fields.Many2one('res.partner', string="Partner")

    @api.onchange('name', 'months')
    def _onchange_name(self):
        if self.name:
            self.months = self.name.months
            self.next_recall_date = fields.Date.today() + relativedelta(months=self.months)


class RecallSchedule(models.Model):
    _name = 'spec.recall.schedule'
    _description = 'Recall Schedule'

    number = fields.Integer(string="Number of Periods")
    period = fields.Selection(
        [('day', 'Day'), ('week', 'Week'), ('month', 'Month'), ], string="Period")
    when = fields.Selection(
        [('before', 'Before'), ('after', 'After')], default='before', string="When")
    recall_type_id_2 = fields.Many2one(
        'spec.recall.type', string="Recall Type")
    recall_type_id = fields.Many2one('spec.recall.type', string="Recall Type")
    partner_id = fields.Many2one('res.partner', string="Partner")


class ContectLens(models.Model):
    _name = 'spec.contact.lenses'
    _description = 'Contect Lens'

    name = fields.Many2one('hr.employee', string="Provider")
    rx_type_char = fields.Char(string="Rx Char")
    rx = fields.Selection([('glasses', 'Glasses'), ('soft', 'Soft Contact Lens'), ('hard', 'Hard Contact Lens')], string="Rx", default='glasses')
    provide = fields.Selection([('provide', 'Provider'), ('outside_provide', 'Outside Provider')], default='provide', string="Provider")
    rx_usage = fields.Selection([('distance', 'Distance'), ('computer', 'Computer'), ('computer_over_contacts', 'Computer Over Contacts'),
                                 ('occupational', 'Occupational'), ('ready_over_contacts', 'Ready Over Contacts'), ('full_time_wear,', 'Full Time Wear'),
                                 ('sports_hobby', 'Sports/Hobby')], string="Rx Usage")
    discontinue_reason = fields.Selection([('recheck', 'Recheck'), ('pt_request', 'PT Request')], string="Discontinue Reason")
    discontinue_date = fields.Date(string="Discontinue Date", default=fields.Date.today())
    exam_date = fields.Date(string="Exam Date", default=fields.Date.today())
    expiration_date = fields.Date(string="Expiration Date")
    """Soft Conatct Lens Fields"""
    soft_manufacturer_id = fields.Many2one('product.template', domain="[('spec_product_type','=','contact_lens')]", string="Manufacturer")
    soft_style = fields.Char(string="Style")
    soft_color = fields.Char(string="Color")
    soft_base_curve = fields.Char(string="Base Curve")
    soft_diameter = fields.Char(string="Diameter")
    soft_sphere = fields.Char(string="Sphere")
    soft_cylinder = fields.Char(string="Cylinder")
    soft_axis = fields.Char(string="Axis")
    soft_add_power = fields.Char(string="Add Power")
    soft_va = fields.Char(string="VA")
    soft_multifocal = fields.Char(string="Multifocal")
    soft_left_manufacturer_id = fields.Many2one('product.template', domain="[('spec_product_type','=','contact_lens')]", string="Manufacturer")
    soft_left_style = fields.Char(string="Style")
    soft_left_color = fields.Char(string="Color")
    soft_left_base_curve = fields.Char(string="Base Curve")
    soft_left_diameter = fields.Char(string="Diameter")
    soft_left_sphere = fields.Char(string="Sphere")
    soft_left_cylinder = fields.Char(string="Cylinder")
    soft_left_axis = fields.Char(string="Axis")
    soft_left_add_power = fields.Char(string="Add Power")
    soft_left_va = fields.Char(string="VA")
    soft_left_multifocal = fields.Char(string="Multifocal")
    wearing_schedulen = fields.Many2one('spec.contact.lens.wear.period', string="Wearing Period")
    replcement = fields.Many2one('spec.contact.lens.replacement.schedule', string="Replacement")
    """Hard Contact Lens Fields"""
    """OS"""
    manufacturer_id = fields.Many2one('product.template', domain="[('spec_product_type','=','contact_lens')]", string="Manufacturer")
    style = fields.Char(string="Name")
    material = fields.Char(string="Material")
    base_curve = fields.Char(string="BC")
    diameter = fields.Char(string="Diameter")
    sphere = fields.Char(string="Sphere")
    cylinder = fields.Char(string="Cylinder")
    axis = fields.Char(string="Axis")
    add = fields.Char(string="Add")
    seg_height = fields.Char(string="Seg  Ht")
    pc_radius = fields.Char(string="PC Radius")
    pc_width = fields.Char(string="PC width")
    ct = fields.Char(string="CT")
    oz = fields.Char(string="OZ")
    base_curve_2 = fields.Char(string="BC 2")
    color = fields.Selection([('clear', 'Clear'), ('dlue', 'Blue')], string="Color")
    sphere_2 = fields.Char(string="Sph 2")
    axis_2 = fields.Char(string="Axis 2")
    cylinder_2 = fields.Char(string="Cyl 2")
    add_diam_2 = fields.Char(string="Add Diam")
    dot = fields.Boolean(string="Dot")
    """OD"""
    left_manufacturer_id = fields.Many2one('product.template', domain="[('spec_product_type','=','contact_lens')]", string="Manufacturer")
    left_style = fields.Char(string="Name")
    left_material = fields.Char(string="Material")
    left_base_curve = fields.Char(string="BC")
    left_diameter = fields.Char(string="Diameter")
    left_sphere = fields.Char(string="Sphere")
    left_cylinder = fields.Char(string="Cylinder")
    left_axis = fields.Char(string="Axis")
    left_add = fields.Char(string="Add")
    left_seg_height = fields.Char(string="Seg Ht")
    left_pc_radius = fields.Char(string="PC Radius")
    left_ct = fields.Char(string="CT")
    left_oz = fields.Char(string="OZ")
    left_pc_width = fields.Char(string="PC width")
    left_base_curve_2 = fields.Char(string="BC 2")
    left_color = fields.Selection([('clear', 'Clear'), ('dlue', 'Blue')], string="Color")
    left_sphere_2 = fields.Char(string="Sph 2")
    left_cylinder_2 = fields.Char(string="Cyl 2")
    left_axis_2 = fields.Char(string="Axis 2")
    left_add_diam_2 = fields.Char(string="Add Diam")
    left_dot = fields.Boolean(string="Dot", default=True)
    """Glasses Fileds"""
    gls_sphere = fields.Selection(selection=lambda self: self.get_sphere_list(), string='Sphere')
    gls_cylinder = fields.Selection(selection=lambda self: self.get_cylinder_list(), string='Cylinder')
    gls_axis = fields.Selection(selection=lambda self: self.get_axis_list(), string='Axis')
    gls_add = fields.Selection(selection=lambda self: self.get_add_list(), string='Add')
    gls_h_prism = fields.Char(string="H.Prism")
    gls_v_prism = fields.Char(string="V.Prism")
    gls_h_base = fields.Selection([('bi', 'BI'), ('bo', 'BO')], string="H.Base")
    gls_v_base = fields.Selection([('bu', 'BU'), ('bd', 'BD')], string="V.Base")
    gls_pd = fields.Char(string="pd")
    gls_va = fields.Selection([('20_10', '20/10'), ('20_20 ', '20/20'), ('20_30', '20/30'), ('20_40 ', '20/40'), ('20_50', '20/50'),
                               ('20_60 ', '20/60'), ('20_80', '20/80'), ('20_100', '20/100'), ('20_150', '20/150'), ('20_200', '20/200'),
                               ('20_400', '20/400'), ('20_800', '20/800')], string="VA")
    gls_near = fields.Selection([('20_10', '20/10'), ('20_20 ', '20/20'), ('20_30', '20/30'), ('20_40 ', '20/40'), ('20_50', '20/50'),
                                 ('20_60 ', '20/60'), ('20_80', '20/80'), ('20_100', '20/100'), ('20_150', '20/150'), ('20_200', '20/200'),
                                 ('20_400', '20/400'), ('20_800', '20/800')], string="VA Near")
    gls_balance = fields.Boolean(string="Balance")
    gls_sub_cff = fields.Boolean(string="Slub Off")
    gls_left_lens_sphere = fields.Selection(selection=lambda self: self.get_sphere_list(),  string='Sphere')
    gls_left_lens_cylinder = fields.Selection(selection=lambda self: self.get_cylinder_list(),  string='Cylinder')
    gls_left_lens_axis = fields.Selection(selection=lambda self: self.get_axis_list(), string='Axis')
    gls_left_lens_add = fields.Selection(selection=lambda self: self.get_add_list(), string='Add')
    gls_left_lens_h_prism = fields.Char(string="H.Prism")
    gls_left_lens_v_prism = fields.Char(string="V.Prism")
    gls_left_h_base = fields.Selection([('bi', 'BI'), ('bo', 'BO')], string="H.Base")
    gls_left_v_base = fields.Selection([('bu', 'BU'), ('bd', 'BD')], string="V.Base")
    gls_left_pd = fields.Char(string="pd")
    gls_left_va = fields.Selection([('20_10', '20/10'), ('20_20 ', '20/20'), ('20_30', '20/30'), ('20_40 ', '20/40'), ('20_50', '20/50'),
                                    ('20_60 ', '20/60'), ('20_80', '20/80'), ('20_100', '20/100'), ('20_150', '20/150'), ('20_200', '20/200'),
                                    ('20_400', '20/400'), ('20_800', '20/800')], string="VA")
    gls_left_near = fields.Selection([('20_10', '20/10'), ('20_20 ', '20/20'), ('20_30', '20/30'), ('20_40 ', '20/40'), ('20_50', '20/50'),
                                      ('20_60 ', '20/60'), ('20_80', '20/80'), ('20_100', '20/100'), ('20_150', '20/150'), ('20_200', '20/200'),
                                      ('20_400', '20/400'), ('20_800', '20/800')], string="VA Near")
    gls_left_balance = fields.Boolean(string="Balance")
    gls_left_lens_sub_cff = fields.Boolean(string="Slab Off")
    gls_left_lens_base_curve = fields.Char(string="Base Curve")
    gls_distant = fields.Selection([('distance', 'Distance'), ('intermediate_200', 'Intermediate 200cm/80in'), ('intermediate_100', 'Intermediate 100cm/40in'),
                                    ('intermediate_65', 'Intermediate 65cm/25in'), ('reading_50', 'Reading 50cm/20in,'), ('reading_40', 'Reading 40 cm/16in'),
                                    ('reading_35', 'Reading 35cm/14in')], string="Rx Distant")
    gls_left_near = fields.Char(string="VA Near")
    gls_lens_style_id = fields.Many2one('spec.lens.style', string="Lens Style")
    gls_lens_material_id = fields.Many2one('spec.lens.material', string="Lens Material")
    gls_ar_coating = fields.Boolean(string="AR Coating")
    gls_photochromic = fields.Boolean(string="Photochromic")
    gls_polarized = fields.Boolean(string="Polarized")
    gls_tint = fields.Boolean(string="Tint")
    rx_notes = fields.Text(string="Rx Notes")
    partner_id = fields.Many2one('res.partner', string="Partner")

    @api.onchange('soft_manufacturer_id', 'soft_left_manufacturer_id')
    def _onchange_manufacturer(self):
        """ Onchnage on Manufacture id for soft or right and left and right lens"""
        if self.soft_manufacturer_id:
            self.soft_style = self.soft_manufacturer_id.name
            for configurations_id in self.soft_manufacturer_id.configurations_ids:
                self.soft_base_curve = configurations_id.bc
                self.soft_diameter = configurations_id.diam
                self.soft_sphere = configurations_id.sphere
                self.soft_cylinder = configurations_id.cylinder
                self.soft_axis = configurations_id.axis
                self.soft_add_power = configurations_id.add
                self.soft_color = configurations_id.color
                self.soft_multifocal = configurations_id.multi_focal
        if self.soft_left_manufacturer_id:
            self.soft_left_style = self.soft_left_manufacturer_id.name

    @api.onchange('manufacturer_id', 'left_manufacturer_id', 'exam_date', 'rx')
    def _onchange_hard_manufacturer(self):
        if self.manufacturer_id:
            self.style = self.manufacturer_id.name
            for configurations_id in self.manufacturer_id.configurations_ids:
                self.base_curve = configurations_id.bc
                self.diameter = configurations_id.diam
                self.sphere = configurations_id.sphere
                self.cylinder = configurations_id.cylinder
                self.axis = configurations_id.axis
                self.style = self.manufacturer_id.name
        if self.left_manufacturer_id:
            self.style = self.left_manufacturer_id.name
            for configurations_id in self.manufacturer_id.configurations_ids:
                self.left_base_curve = configurations_id.bc
                self.left_diameter = configurations_id.diam
                self.left_sphere = configurations_id.sphere
                self.left_cylinder = configurations_id.cylinder
                self.left_axis = configurations_id.axis
                self.left_style = self.manufacturer_id.name

    @api.onchange('exam_date', 'rx')
    def _onchange_exam_date(self):
        if self.rx == 'glasses':
            self.expiration_date = self.exam_date + relativedelta(years=1)
        else:
            self.expiration_date = self.exam_date + \
                relativedelta(years=2)

    @api.onchange('discontinue_reason')
    def onchange_discontinue_reason(self):
        if self.discontinue_reason:
            self.discontinue_date = fields.Date.today()
        else:
            self.discontinue_date = False

    def soft_copy_to_left(self):
        if self.soft_manufacturer_id:
            self.soft_left_manufacturer_id = self.soft_manufacturer_id
            self.soft_left_style = self.soft_style
            self.soft_left_color = self.soft_color
            self.soft_left_base_curve = self.soft_base_curve
            self.soft_left_diameter = self.soft_diameter
            self.soft_left_sphere = self.soft_sphere
            self.soft_left_cylinder = self.soft_cylinder
            self.soft_left_axis = self.soft_axis
            self.soft_left_add_power = self.soft_add_power
            self.soft_left_multifocal = self.soft_multifocal
            self.soft_left_va = self.soft_va

    def copy_to_left_lens(self):
        if self.gls_sphere:
            self.gls_left_lens_sphere = self.gls_sphere
            self.gls_left_lens_cylinder = self.gls_cylinder
            self.gls_left_lens_axis = self.gls_axis
            self.gls_left_lens_add = self.gls_add

    def action_convert(self):
        sphere = ''
        left_sphere = ''
        if self.gls_sphere and self.gls_cylinder:
            sphere = str((float(self.gls_sphere)) + (float(self.gls_cylinder)))
            if (float(sphere)) < -30 or (float(sphere)) > 30:
                raise ValidationError('The addition of Sphere and Cylinder between -30.0 and 30.00')
            else:
                self.gls_sphere = sphere
        if float(self.gls_cylinder) and float(self.gls_cylinder) > 0.0:
            self.gls_cylinder = str(-float(self.gls_cylinder))
        if int(self.gls_axis) <= 90:
            self.gls_axis = str(90 + int(self.gls_axis))
        else:
            self.gls_axis = str(int(self.gls_axis) - 90)
        if self.gls_left_lens_sphere and self.gls_left_lens_cylinder:
            left_sphere = str((float(self.gls_left_lens_sphere)) + (float(self.gls_left_lens_cylinder)))
            if (float(left_sphere)) < -30 or (float(left_sphere)) > 30:
                raise ValidationError('The addition of Sphere and Cylinder between -30.0 and 30.00')
            else:
                self.gls_left_lens_sphere = left_sphere
        if float(self.gls_left_lens_cylinder) and float(self.gls_left_lens_cylinder) > 0.0:
            self.gls_left_lens_cylinder = str(-float(self.gls_left_lens_cylinder))
        if int(self.gls_left_lens_axis) < 90:
            self.gls_left_lens_axis = str(90 + int(self.gls_left_lens_axis))
        else:
            self.gls_left_lens_axis = str(int(self.gls_left_lens_axis) - 90)

    @api.onchange('rx', 'rx_usage')
    def _onchange_name(self):
        name = ''
        if self.rx:
                rx = dict(self.fields_get(["rx"],['selection'])['rx']["selection"]).get(self.rx)
                name += rx or ''
        if self.rx_usage:
            rx_usage = dict(self.fields_get(["rx_usage"],['selection'])['rx_usage']["selection"]).get(self.rx_usage)
            name += ' (' + rx_usage + ') ' or ''
        self.rx_type_char = name

    def get_sphere_list(self):
        list1 = [((str(list_value/100)), (list_value/100)) for list_value in range(-3000, 3025, 25)] or []
        return list1

    def get_cylinder_list(self):
        list1 = [((str(list_value/100)), (list_value/100)) for list_value in range(-2000, 2025, 25)] or []
        return list1

    def get_axis_list(self):
        list1 = [((str(list_value)), (list_value)) for list_value in range(-000, 181, 1)] or []
        return list1

    def get_add_list(self):
        list1 = [((str(list_value/100)), (list_value/100)) for list_value in range(-25, 625, 25)] or []
        return list1


class Appointment(models.Model):
    _name = 'spec.appointment'
    _description = 'Appointment'
    _rec_name = 'patient_id'

    patient_id = fields.Many2one('res.partner', string="Patient")
    service_type = fields.Many2one(
        'product.template', domain="[('spec_product_type','=','service')]", string="Service Type")
    insurance_id = fields.Many2one('spec.insurance', string="Insurance")
    phone = fields.Char(string="Phone")
    user_id = fields.Many2one(
        'res.users', string='Resource', default=lambda self: self.env.user, readonly="1")
    pre_appointment = fields.Boolean(string="Pre-Appointment")
    appointment_date = fields.Date(
        string="Appointment Date")
    appointment_time = fields.Float(string="Appointment Time")
    appointment_duration = fields.Integer(string="Appointment Duration")
    confirmation_status = fields.Selection([('none', 'None'), ('left_message', 'Left Message'), ('not_available', 'Not Available'),
                                            ('confirmed', 'Confirmed')], default='none', string="Confirmation Status")
    appointment_status = fields.Selection([('none', 'None'), ('no_show', 'No Show'), ('checked_in', 'Checked In'), ('checked_out', 'Checked Out'),
                                           ('walk_in', 'Walk In'), ('cancel', 'Cancel')], default='none', string="Appointment Status")
    frequency = fields.Selection([('one_time', 'One time'), ('recurs_weekly', 'Recurs weekly'), ('recurs_every_two_weeks', 'Recurs every two weeks'),
                                  ('recurs_every_three_weeks', 'Recurs every three weeks'), ('recurs_every_four_weeks', 'Recurs every four weeks')], default='one_time', string="Frequency")
    notes = fields.Text(string="Appointment Notes")
    urgent = fields.Boolean(string="Urgent")
    follow_up = fields.Boolean(string="Follow Up")
    monday = fields.Boolean('Monday')
    tuesday = fields.Boolean('Tuesday')
    wednesday = fields.Boolean('Wednesday')
    thursday = fields.Boolean('Thursday')
    friday = fields.Boolean('Friday')
    saturday = fields.Boolean('Saturday')
    sunday = fields.Boolean('Sunday')
    employee_id = fields.Many2one('hr.employee', string="Clinician")
    street = fields.Char(
        related='employee_id.work_location', string="Location", store=True)
    telehealth = fields.Boolean(string="Telehealth")
    total_appointment = fields.Integer(
        compute="_get_total_appointment", string="Total Appointment")
    total_confirmation = fields.Integer(string="Confirmation")
    total_none = fields.Integer(
        compute="_get_total_none", string="Total Confirmation")

    @api.onchange('patient_id')
    def _onchange_name(self):
        for rec in self.patient_id.insurance_ids:
            if rec.primary_insurance:
                self.insurance_id = rec.id
        if self.patient_id:
            self.phone = self.patient_id.phone

    @api.depends('patient_id',)
    def _get_total_appointment(self):
        self.total_appointment = self.env['spec.appointment'].search_count([])

    @api.depends('appointment_status',)
    def _get_confirmation_status(self):
        self.total_confirmation = self.env['spec.appointment'].search_count(
            [('confirmation_status', '=', 'confirmed')])

    @api.depends('appointment_status')
    def _get_total_none(self):
        self.total_none = self.env['spec.appointment'].search_count(
            [('confirmation_status', '=', 'none')])


class ResourceCalendarLeaves(models.Model):
    _inherit = 'resource.calendar.leaves'

    date_from = fields.Date('Start Date', required=True)
    date_to = fields.Date('End Date', required=True)
