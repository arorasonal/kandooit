# -*- coding: utf-8 -*-

from odoo import fields, models, api
from datetime import date


class HrEmployee(models.Model):
    _inherit = "hr.employee"

    provide = fields.Selection(
        [('outside_provide', 'Outside'), ('provide', 'In-House')], default='outside_provide', string="outside")
    first_name = fields.Char(string="Frist Name")
    last_name = fields.Char(string="Last Name")
    credential = fields.Selection([('od', 'OD'), ('md', 'MD'),
                                   ('do', 'DO'), ('abo', 'ABO'), ('Staff', 'Staff')], string="Credential")
    signature = fields.Binary(string="Signature")
    signature_date = fields.Date(string="Signature Date")
    fax = fields.Char(string="Fax")
    ein = fields.Integer(string="EIN")
    license = fields.Char(string="License")
    dea = fields.Char(string="DEA")
    npi = fields.Char(string="NPI")
    npi_type = fields.Selection(
        [('group', 'Group'), ('individual', 'Individual')], default="group", string="NPI type")
    taxonomy = fields.Char(string="Taxonomy")
    online_appointment = fields.Boolean(string='Online Appointment')
    appointment = fields.Boolean(
        string='Appointment')
    allow_overbooks = fields.Integer(string="Allow Overbooks")
    duration = fields.Float(string="Duration")
    insurance_ids = fields.One2many(
        'spec.employee.insurance', 'employee_id', string="Insurance")
    color = fields.Char(string="Color")

    @api.onchange('first_name', 'last_name')
    def _onchange_name(self):
        name = ''
        if self.last_name:
            name += self.last_name + ' ' or ''
        if self.first_name:
            name += self.first_name + ' ' or ''
        self.name = name


class EmployeeInsurance(models.Model):
    _name = 'spec.employee.insurance'
    _description = 'Employee Insurance'

    name = fields.Char(string="Insurance")
    npi_type = fields.Selection(
        [('group', 'Group'), ('individual', 'Individual')], default="group", string="NPI type")
    billing_npi = fields.Integer(string="Billing NPI")
    rendering_provider_npi = fields.Integer(string="Rendering Provider NPI")
    tax_id = fields.Integer(string="Tax ID")
    employee_id = fields.Many2one('hr.employee', string="Employee")
