# -*- coding: utf-8 -*-

from odoo import fields, models, api
from odoo.exceptions import UserError


class ContactLens(models.Model):
    _inherit = "product.template"

    image = fields.Image(string="Image", attachment=True)
    discontinue = fields.Boolean(string='Discontinued')
    discont_date = fields.Date(string='Discontinue Date')
    contact_lens_manufacturer_id = fields.Many2one('spec.contact.lens.manufacturer', string='Manufacturer')
    units_per_box = fields.Char(string='Units Per Box', default=6)
    lens_type_id = fields.Many2one(
        'spec.contact.lens.type', string='Lens Type')
    color_type_id = fields.Many2one(
        'spec.contact.lens.color.type', string='Color Type')
    wear_period_id = fields.Many2one(
        'spec.contact.lens.wear.period', string='Wear Period')
    replacement_schedule_id = fields.Many2one(
        'spec.contact.lens.replacement.schedule', string="Replacement Schedule")
    trial_lens_id = fields.Many2one('product.template', domain="[('spec_product_type', '=', 'contact_lens')]" ,string="Trial Lens")
    contact_lens_summary_ids = fields.One2many('spec.contact.lens.summary', 'contact_lens_id',
                                               string="Contact Lens Summary")
    configurations_ids = fields.One2many(
        'spec.contact.lens.configurations', 'contact_lens_id', string="Configurations")
    material = fields.Char(string="Material")
    fda_group = fields.Char(string="FDA Group")
    water_content = fields.Char(string="Water Content")
    dk = fields.Char(string="DK")
    modulus = fields.Char(string="Modulus")
    ct = fields.Char(string="CT")
    oz = fields.Char(string="OZ")
    uv_blocking = fields.Char(string="UV Blocking")
    manuf_process = fields.Char(string="Manuf. Process")
    appearance = fields.Char(string="Appearance")
    toric_type = fields.Char(string="Toric Type")
    indication_id = fields.Many2many('spec.contact.lens.indication', 'contact_lens_indication_id'
                                     'contact_lens_id', 'indication_id', string="Indication")
    cost = fields.Float(string="Cost")
    con_lens_suggested_retail = fields.Float(string="Suggested  Retail")
    procedure_code = fields.Many2one('spec.procedure.code', string='Procedure')
    fitting_guide_or_package_insert = fields.Binary(
        string="Fitting Guide or Package Insert")
    fitting_guide_or_package_insert_file_name = fields.Char(string='File Name')
    available_rebate = fields.Binary(string="Available Rebate")
    available_rebate_file_name = fields.Char(string='File Name')
    website = fields.Char(String="Website")
    note = fields.Text(string='Notes')

    @api.constrains('available_rebate_file_name')
    def check_available_rebate_file_name(self):
        """Check available rebate file only PDF allow"""
        for record in self:
            if record.available_rebate_file_name:
                file_name = record.available_rebate_file_name.split('.')
                if file_name[-1].lower() != 'pdf':
                    raise UserError("You can upload only PDF file!")

    @api.constrains('fitting_guide_or_package_insert')
    def check_fitting_guide_or_package_insert(self):
        """Check Fitting guide or package insert only PDF allow"""
        for record in self:
            if record.fitting_guide_or_package_insert_file_name:
                file_name = record.fitting_guide_or_package_insert_file_name.split(
                    '.')
                if file_name[-1].lower() != 'pdf':
                    raise UserError("You can upload only PDF file!")

    @api.onchange('discontinue')
    def onchange_discontinue(self):
        """Onchange on discontinue"""
        self.discont_date = self.discontinue and fields.Date.today() or False

    def name_get(self):
        result = []
        for lens in self:
            manufacturer = lens.contact_lens_manufacturer_id.name if lens.contact_lens_manufacturer_id else ''
            name = manufacturer + ' ' + lens.name
            result.append((lens.id, name))
        return result


class ContactLensSummary(models.Model):
    _name = 'spec.contact.lens.summary'
    _description = 'Contact Lens Summary'

    bc = fields.Char(string='BC')
    diam = fields.Float(string='Diam')
    sphere_from = fields.Float(string='Sphere From')
    sphere_to = fields.Float(string='Sphere To')
    sphere_step = fields.Float(string='Sphere Step')
    contact_lens_id = fields.Many2one('product.template', domain="[('spec_product_type', '=', 'contact_lens')]", string="Contact Lens ID")


class ContactLensConfigurations(models.Model):
    _name = 'spec.contact.lens.configurations'
    _description = 'Contact Lens configurations'
    _rec_name = 'bc'

    bc = fields.Char(string='BC')
    bc_id = fields.Char(string='BC')
    diam = fields.Float(string='Diam')
    sphere = fields.Float(string='Sphere')
    cylinder = fields.Float(string='Cylinder')
    axis = fields.Char(string='Axis')
    add = fields.Char(string='Add')
    multi_focal = fields.Char(string='Multi-focal')
    color = fields.Char(string='Color')
    upc = fields.Char(string='UPC')
    contact_lens_id = fields.Many2one('product.template', domain="[('spec_product_type', '=', 'contact_lens')]", string="Contact Lens ID")


class ContactLensType(models.Model):
    _name = 'spec.contact.lens.type'
    _description = 'Lens Style'

    name = fields.Char(string='Lens Type')


class ContactLensColorType(models.Model):
    _name = 'spec.contact.lens.color.type'
    _description = 'Contact Lens Color Style'

    name = fields.Char(string='Color Type')


class ContactWearPeriod(models.Model):
    _name = 'spec.contact.lens.wear.period'
    _description = 'Contact Lens Wear Period'

    name = fields.Char(string='Wear Period')


class ContactLensReplacementSchedule(models.Model):
    _name = 'spec.contact.lens.replacement.schedule'
    _description = 'Contact Lens Replacement schedule'

    name = fields.Char(string='Replacement Schedule')


class ContactLensIndiaction(models.Model):
    _name = 'spec.contact.lens.indication'
    _description = 'Contact Lens Indication'

    name = fields.Char(string='Indication')


class ContactLensManufacturer(models.Model):
    _name = 'spec.contact.lens.manufacturer'
    _description = 'Contact Lens Manufacturer'

    name = fields.Char(string='Manufacturer')
    